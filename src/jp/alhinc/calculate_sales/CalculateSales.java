package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
//BufferdWriterをインポート　3-1
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
//FileWriterをインポート　3-1
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String BRANCH = "支店定義ファイル";
	private static final String NOT_CONTINUE = "売上ファイル名が連番になっていません";
	private static final String OVER_10_DIGITS = "合計金額が10桁を超えました";
	private static final String ILLEGAL_BRANCH_CODE = "の支店コードが不正です";
	private static final String ILLEGAL_FORMAT = "のフォーマットが不正です";
	private static final String COMMODITY = "商品定義ファイル";
	private static final String ILLEGAL_COMMODITY_CODE = "の商品コードが不正です";
	private static final String NOT_EXIST = "が存在しません";

	//条件式
	private static final String numberJude = "^[0-9]{3}$";
	private static final String alphabetNumber = "^[A-Za-z0-9]{8}$";
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		// コマンドライン引数が渡されているか確認
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales,
				numberJude, BRANCH)) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales,
				alphabetNumber, COMMODITY)) {
			return;
		}

		// 集計処理開始
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for (int i = 0; i < files.length; i++) {
			String commonPoint = "^[0-9]{8}.rcd$";

			// 条件を満たしているか確認
			if (files[i].isFile() && files[i].getName().matches(commonPoint)) {
				rcdFiles.add(files[i]);
			}

		}

		// 連番なのか確認
		for (int k = 0; k < rcdFiles.size() -1; k++) {
			int former = Integer.parseInt(files[k].getName().substring(0,8));
			int latter= Integer.parseInt(files[k + 1].getName().substring(0,8));

			if ((latter - former) != 1) {
				System.out.println(NOT_CONTINUE);
				return;
			}

		}

		// 支店コード、、商品コード、売上額の抽出
		for (int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				List<String> sales = new ArrayList<>();
				String line;

				while((line = br.readLine()) != null) {
					sales.add(line);
				}

				// 売上ファイルのフォーマットが正しいか確認
				if (sales.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() +ILLEGAL_FORMAT);
					return;
				}

				// 売上金額が数字なのか確認
				if (!sales.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				Long fileSales = Long.parseLong(sales.get(2));
				Long branchSalesAmount = branchSales.get(sales.get(0)) + fileSales;
				Long commoditySalesAmount = commoditySales.get(sales.get(1))+ fileSales;

				// 売上ファイルの支店コードが支店定義ファイルに存在するか確認
				if (!branchSales.containsKey(sales.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + ILLEGAL_BRANCH_CODE);
					return;
				}

				// 売上ファイルの商品コードが商品定義ファイルに存在するか確認
				if (!commoditySales.containsKey(sales.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + ILLEGAL_COMMODITY_CODE);
					return;
				}

				//集計金額が10桁を超えているか確認(支店、商品)
				if ((branchSalesAmount >= 10000000000L) || (commoditySalesAmount >= 10000000000L)) {
					System.out.println(OVER_10_DIGITS);
					return;
				}

				branchSales.put(sales.get(0),branchSalesAmount);
				commoditySales.put(sales.get(1), commoditySalesAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理 && 商品定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> thingsNames,
			Map<String, Long> thingsSales, String condition, String things) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			// ファイルの有無の確認
			if (!file.exists()) {
				System.out.println(things + NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {

				String[] items = line.split(",");

				// 条件分岐
				if ((items.length != 2) || (!items[0].matches(condition))) {
					System.out.println(things + ILLEGAL_FORMAT);
					return false;
				}

				thingsNames.put(items[0],items[1]);
				thingsSales.put(items[0], 0L);

			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> thingsNames,
			Map<String, Long> thingsSales) {
		File file = new File(path,fileName);
		BufferedWriter bw = null;

		try {
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for (String key: thingsSales.keySet()) {
				bw.write(key + "," + thingsNames.get(key) + "," + thingsSales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
